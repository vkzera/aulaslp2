﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercicio_0503_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cliente cliente = new Cliente();
            Banco banco = new Banco();
            Conta conta = new Conta();
            Conta conta1 = new Conta();

            cliente.Nome = "Victor";
            cliente.Cpf = 123456;

            banco.NumBanco = 123;

            conta.NumAgencia = 456;
            conta.NumConta = 789;
            conta.Limite = 5000;
            conta.Saldo = 2831;

            conta1.NumAgencia = 111;
            conta1.NumConta = 222;
            conta1.Limite = 55500;
            conta1.Saldo = 231;

            cliente.Contas.Add(conta);
            cliente.Contas.Add(conta1);
            banco.Contas.Add(conta);
            conta.Cliente = cliente;
            conta.Banco = banco;

            Console.WriteLine("Listando contas do cliente {0}:", cliente.Nome);
            cliente.ListarContas();
           
            Console.Read();
            

        }
    }
}
