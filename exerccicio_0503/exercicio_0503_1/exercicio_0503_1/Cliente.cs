﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercicio_0503_1
{
    class Cliente
    {
        private string nome;
        private long cpf;
        private List<Conta> contas = new List<Conta>();
        internal List<Conta> Contas
        {
            get
            {
                return contas;
            }

            set
            {
                contas = value;
            }
        }

        public string Nome { get; set; }
        public long Cpf { get; set; }


        public void ListarContas()
        {
            
            foreach (Conta conta in this.contas)
            {
                Console.WriteLine("============================================");
                Console.WriteLine("Número da conta: {0}\nAgência conta: {1}\nSaldo: {2}\nTempo conta: {3}", conta.NumConta, conta.NumAgencia, conta.Saldo, conta.TempoConta);
                Console.WriteLine("============================================");
            }

        }


    }
}
