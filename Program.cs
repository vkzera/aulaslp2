﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercicio_2602_1
{
    class Program
    {
        public const int MAX = 10;

        static void Main(string[] args)
        {
            string[] palavras = new string[MAX];
            int posicaoMenorPalavra = 0, posicaoMaiorPalavra = 0;
            Boolean possuiE, possuiA;
            


            for (int i = 0; i <MAX; i++)
            {          
                Console.Write("Digite a {0}º palavra: ", i + 1);
                palavras[i] = Console.ReadLine();
            }
            

            for (int i = 0; i<MAX; i++)
            {
                if (i == 0)
                {
                    posicaoMaiorPalavra = 0;
                    posicaoMenorPalavra = 0;
                }
                else
                {
                    if(i < MAX)
                    { 
                     if (palavras[posicaoMenorPalavra].Length > palavras[i].Length)
                    {
                        posicaoMenorPalavra = i;

                    }
                    if (palavras[posicaoMaiorPalavra].Length < palavras[i].Length)
                    {
                        posicaoMaiorPalavra = i;

                    }
                    }
                    if(i == MAX)
                    {
                        if(palavras[i].Length < palavras[i + 1].Length)
                        {
                            posicaoMenorPalavra = i;

                        }
                        if (palavras[i].Length > palavras[i + 1].Length)
                        {
                            posicaoMaiorPalavra = i;

                        }
                    }




                }

                Console.Write("\n Palavra {0} original: {1}", i+1, palavras[i]);
                string palavra = palavras[i];
                string palavraInvertida = "";

                for (int j = palavra.Length - 1; j >= 0; j--)
                {
                    palavraInvertida += palavra[j];
                                                        
                }
                Console.WriteLine("Palavra invertida: {0}\n", palavraInvertida);               
                Console.WriteLine();            
  
            }

            Console.WriteLine("Maior palavra: {0} na posição {1} do vetor\n", palavras[posicaoMaiorPalavra], posicaoMaiorPalavra);
            Console.WriteLine("Menor palavra: {0} na posição {1} do vetor\n", palavras[posicaoMenorPalavra], posicaoMenorPalavra);
            Console.WriteLine("Palavras com a letra a: \n");

            foreach(string x in palavras)
            {
                if (x.StartsWith("a"))
                {

                    Console.Write(x + "\t");
                }
            }

            Console.WriteLine("\nFrase: ");
            foreach(string p in palavras)
            {
                if (p.EndsWith("e"))
                {
                    
                    Console.Write(p + "\t");
                }
            }
            Console.Read();       
    }
    }
}
